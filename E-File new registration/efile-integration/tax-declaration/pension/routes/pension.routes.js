'use strict';

const Controller = require('../controller'),
           pension=Controller.pensionCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/pension/info',
        method:'GET',
        handler:pension.getpension
    },
    {
        path:'/pension/save',
        method:'POST',
        handler:pension.createpension
    },
    {
        path:'/pension/edite',
        method:'PUT',
        handler:pension.editpension
       
    },
    {
        path:'/pension/remove',
        method:'DELETE',
        handler:pension.deletepension
        } 
]; 
