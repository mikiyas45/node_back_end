'use strict';

const Boom = require('boom'),
      //Model = require('../model'),
      Model = require('../../model'),
      mongoose=require('mongoose'),
    // Config = require('../../config') 
    pensionData = Model.taxModel
      
 class pension {
    createpension(request,reply){
        const pension=new pensionData({
         _id: new mongoose.Types.ObjectId(),
         number_of_transaction: request.payload.number_of_transaction,
         totalTaxebleAmount: request.payload.totalTaxebleAmount,
         totalTaxWithheld: request.payload.totalTaxWithheld,
            tax_details: request.payload.tax_details,
             tin: request.payload.tax_details.tin,
             full_name: request.payload.tax_details.full_name,
             desc: request.payload.tax_details.desc,
            taxableincome: request.payload.tax_details.taxableincome,
            tax_withheld: request.payload.tax_details.tax_withheld,
        });
        pension.save() 
        .then(data => reply(data))
        .catch(err => {
          request.logger.error(err);
          return reply(Boom.internal('An error occurred. Please try again later.'));
        }); 
    }
    editpension(request,reply){ 
        pensionData.findByIdAndUpdate({_id:request.payload.id},{
         taxpayer_identification:request.payload.taxpayer_identificationId,
          numberOfworkersPaidForthisMonth:request.payload.numberOfworkersPaidForthisMonth,
          theGrossTaxableIncomeForthisMonth:request.payload.theGrossTaxableIncomeForthisMonth,
          costsharingCosts:request.payload.costsharingCosts})
                        .then(data=>!data?reply(Boom.notFound('no costsharing data is saved')):reply('success'))
                        .catch(err=>{
                            request.logger.error(err);
                            return reply(Boom.internal('An error occurred. Please try again later.'));
                        }); 
    }
    deletepension(request,reply){
        pensionData.findByIdAndRemove({_id:request.payload.id})
            .then(data=>!data?reply(Boom.notFound('no costsharing')):reply('success'))
            .catch(err=>{
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            }); 
    }
    getpension(request,reply){
        pensionData.find({},{__v:0,created_at:0})
            .then(data=>{
                if(!data)
                {
                    return reply(Boom.notFound('no success'));
                }
                let result=[];
                data.map(d=>result.push({id:d._id,
                    number_of_transaction: d.number_of_transaction,
                    totalTaxebleAmount: d.totalTaxebleAmount,
                    totalTaxWithheld: d.totalTaxWithheld,
           
                  // tax_details: d.tax_details,
                        tin: d.tax_details.tin,
                        full_name: d.tax_details.full_name,
                        desc: d.tax_details.desc,
                       taxableincome: d.tax_details.taxableincome,
                       tax_withheld: d.tax_details.tax_withheld,
                
                }))
                return reply(result);
            })
            .catch(err=>{
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            }); 
    }

} 
module.exports = new pension();