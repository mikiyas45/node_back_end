'use strict';

const Controller = require('../controller'),
           Costsharing=Controller.costsharingCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/costsharing/info',
        method:'GET',
        handler:Costsharing.getCostsharing
    },
    {
        path:'/costsharing/save', 
        method:'POST',
        handler:Costsharing.createCostsharing
    },
    {
        path:'/costsharing/update',
        method:'PUT',
        handler:Costsharing.editCostsharing
       
    },
    {
        path:'/costsharing/remove',
        method:'DELETE',
        handler:Costsharing.deleteCostsharing
        } 
]; 
