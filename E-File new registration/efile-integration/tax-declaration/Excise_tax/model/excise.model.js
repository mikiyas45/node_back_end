'use strict';

const  mongoose=require('mongoose');
mongoose.Promise = global.Promise; 

const Schema = mongoose.Schema;
const exciseSchema=new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    //tin: { type: mongoose.Schema.Types.ObjectId, ref: 'incometax_identification' },
    
    taxpayer_name: String,
    tin: String,
    tax_acountNumber:Number,
    tax_type: String,
    tax_period: String,

    description: String,
            
            
    exise_details: {
       
          //only for vat declaration 
          catagory_of_product: { type: String, required: false },
          product_details: { type: String, required: false },
          measurment: { type: String, require:false} ,
          monthly_productions_UNIT: { type: String, required: false },
          unit_cost: { type: Number, required: false },
          cost_of_production: { type: Number, required: false },
          excise_tax_rate: { type: Number, required: false },
          tax_to_pay: { type: Number, required: false },
            
           
        
    } 
}
    , {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });

module.exports= mongoose.model('excise',exciseSchema);
