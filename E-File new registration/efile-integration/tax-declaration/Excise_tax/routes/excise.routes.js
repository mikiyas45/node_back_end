'use strict';

const Controller = require('../controller'),
excise=Controller.exciseCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/excise/info',
        method:'GET',
        handler:excise.getexcise
    },
    {
        path:'/excise/save', 
        method:'POST',
        handler:excise.createexcise
    },
    {
        path:'/excise/update',
        method:'PUT',
        handler:excise.editexcise
       
    },
    {
        path:'/excise/remove',
        method:'DELETE',
        handler:excise.deleteexcise
        } 
]; 
