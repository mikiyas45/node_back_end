'use strict';

const Boom = require('boom'),
    //Model = require('../model'),
    Model = require('../model'),
    mongoose = require('mongoose'),
    // Config = require('../../config')
    exciseData = Model.exciseData

class excise {
    createexcise(request, reply) {
        const excise = new exciseData({
            _id: new mongoose.Types.ObjectId(),
            taxpayer_name: request.payload.taxpayer_name,
            tin: request.payload.tin,
            tax_acountNumber: request.payload.tax_acountNumber,
            tax_type: request.payload.tax_type,
            tax_period: request.payload.tax_period,

            description: request.payload.description,

            exise_details: request.payload.exise_details,

                    catagory_of_product: request.payload.exise_details.catagory_of_product,
                    product_details: request.payload.exise_details.product_details,
                    measurment: request.payload.exise_details.measurment,
                    monthly_productionsUNIT: request.payload.exise_details.monthly_productionsUNIT,
                    unit_cost: request.payload.exise_details.unit_cost,
                    cost_of_production: request.payload.exise_details.cost_of_production,
                    excise_tax_rate: request.payload.exise_details.excise_tax_rate,
                    tax_to_pay: request.payload.exise_details.tax_to_pay,

        });
        excise.save()
            .then(data => reply(data))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    editexcise(request, reply) {
        exciseData.findByIdAndUpdate({ _id: request.payload.id }, {

            taxpayer_name: request.payload.taxpayer_name,
            tin: request.payload.tin,
            tax_acountNumber: request.payload.tax_acountNumber,
            tax_type: request.payload.tax_type,
            tax_period: request.payload.tax_period,

            description: request.payload.description,

            exise_details: request.payload.exise_details,

                    catagory_of_product: request.payload.exise_details.catagory_of_product,
                    product_details: request.payload.exise_details.product_details,
                    measurment: request.payload.exise_details.measurment,
                    monthly_productionsUNIT: request.payload.exise_details.monthly_productionsUNIT,
                    unit_cost: request.payload.exise_details.unit_cost,
                    cost_of_production: request.payload.exise_details.cost_of_production,
                    excise_tax_rate: request.payload.exise_details.excise_tax_rate,
                    tax_to_pay: request.payload.exise_details.tax_to_pay,

        })
            .then(data => !data ? reply(Boom.notFound('no costsharing data is saved')) : reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    deleteexcise(request, reply) {
        exciseData.findByIdAndRemove({ _id: request.payload.id })
            .then(data => !data ? reply(Boom.notFound('no costsharing')) : reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    getexcise(request, reply) {
        exciseData.find({}, { __v: 0, created_at: 0 })
            .then(data => {
                if (!data) {
                    return reply(Boom.notFound('no success'));
                }
                let result = [];
                data.map(d => result.push({
                    id: d._id,
          taxpayer_name:d.taxpayer_name,
            tin:d.tin,
            tax_acountNumber: d.tax_acountNumber,
            tax_type: d.tax_type,
            tax_period: d.tax_period,
            description: d.description,

                    catagory_of_product: d.exise_details.catagory_of_product,
                    product_details: d.exise_details.product_details,
                    measurment: d.exise_details.measurment,
                    monthly_productionsUNIT: d.exise_details.monthly_productionsUNIT,
                    unit_cost: d.exise_details.unit_cost,
                    cost_of_production: d.exise_details.cost_of_production,
                    excise_tax_rate: d.exise_details.excise_tax_rate,
                    tax_to_pay: d.exise_details.tax_to_pay,

                }))
                return reply(result);
            })
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }

}
module.exports = new excise();