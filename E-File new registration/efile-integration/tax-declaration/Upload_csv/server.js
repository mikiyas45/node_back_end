var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var server = require('express')();
var fileUpload = require('express-fileupload');
var mongoose = require('mongoose');
var promise =require('promise');
require("./models/Product");
server.use(fileUpload());
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/dbproducts', 
{ useMongoClient: true });
var index = require('./routes/index');
//var users = require('./routes/users');

var server = express();
// view engine setup
//server.set('views', path.join(__dirname, 'views'));
//server.set('view engine', 'pug');

server.use(logger('dev'));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));
server.use(cookieParser());
 
server.use(flash());

 server.use('/', index);
 //server.use('/users', users);

 server.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


server.use(function(err, req, res, next) {

  res.locals.message = err.message;
  res.locals.error = req.server.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = server;
 