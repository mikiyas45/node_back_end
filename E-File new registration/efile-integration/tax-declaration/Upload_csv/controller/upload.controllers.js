'use strict';

const Boom = require('boom'),
 mongoose = require('mongoose'),
 Config = require('../../../../config')
var csv = require('fast-csv');
var zip= require("node-stream-zip");
var AdmZip = require('adm-zip');
var archiver = require('archiver');

var Product = require('../models/Product');

    

exports.post = function (req, res) {
	if (!req.files)
		return res.status(400).send('No files were uploaded.');
	
	var productFile = req.files.file;

	var product = [];

	csv
	 .fromString(productFile.data.toString(), {
		 headers: true,
		 ignoreEmpty: true
	 })
	 .on("data", function(data){
		 data['_id'] = new mongoose.Types.ObjectId();
		 
		 product.push(data);
	 })
	 .on("end", function(){
		Product.create(product, function(err, documents) {
			if (err) throw err;
			
			res.send(product.length + ' product have been successfully uploaded.');
		 });
 
	 });
	 

};                 

