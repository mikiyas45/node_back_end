'use strict';

const Controller = require('../controller'),
IncometaxController=Controller.IncomeTaxCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/Incometax/save',
        method:'POST',
        handler:IncometaxController.createIncometax
    },
    {
        path:'/Incometax/info',
        method:'GET',
        handler:IncometaxController.getIncometax
    }, 
    {
        path:'/Incometax/update',
        method:'PUT',
        handler:IncometaxController.editIncometax
       
    },
    {
        path:'/Incometax/remove',
        method:'DELETE',
        handler:IncometaxController.deleteIncometax
        }
];

 