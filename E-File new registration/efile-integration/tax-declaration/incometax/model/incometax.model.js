'use strict';

const  mongoose=require('mongoose');
mongoose.Promise = global.Promise; 

const Schema = mongoose.Schema;
const CostsharingSchema=new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    //tin: { type: mongoose.Schema.Types.ObjectId, ref: 'incometax_identification' },
    tin: String,
    name: String,
    tax_type: String,
    tax_period: Number,
    tax_year: Number,
    description: String,
    number_of_transaction: Number,
    totalTaxebleAmount: Number,
    totalTaxWithheld: Number,
    tax_details: {
       
          //only for vat declaration 
            code: { type: Number, required: false },
            type: { type: String, required: false },
            code_name: { type: String, required: false },
            desc: { type: String, required: false }, 
            ///for income tax and costsharing 
           tin:  { type:Number ,required:false}, 
            full_name:  { type:String ,required:false},
            basic_sallary: { type:Number ,required:false},
            address: { type:String ,required:false},
            total_transportation_allowance: { type:Number ,required:false},
            total_taxable_transportation_allowance:{ type:Number ,required:false},
            over_time: { type:String ,required:false},
            other_taxable_benefites: { type:String ,required:false},    
            // for tax_withheld     
                    recieptNo:{ type:Number ,required:false},
                    amountOfPayment:{ type:Number ,required:false},
                    checkNumber:{ type:Number ,required:false},
                    chashiersName:{ type:String ,required:false},
                    taxableincome: { type:Number ,required:false},
                    tax_withheld: { type:Number ,required:false},
        
        }
    } 

    , {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });

module.exports= mongoose.model('tax',CostsharingSchema);
