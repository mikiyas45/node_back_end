'use strict';

const Boom = require('boom'),
     // Model = require('../model'),
      Model = require('../../model'),
      mongoose = require('mongoose'),
      IncometaxDataModel = Model.taxModel
    
 class IncometaxController {
    createIncometax(request,reply){
        const Incometax=new IncometaxDataModel({
            _id: new mongoose.Types.ObjectId(),
           // incometax_identification:request.payload.incometax_identificationId,
           number_of_transaction: request.payload.number_of_transaction,
            totalTaxebleAmount: request.payload.totalTaxebleAmount,
            totalTaxWithheld: request.payload.totalTaxWithheld,

            tax_details: request.payload.tax_details,

            tin: request.payload.tax_details.tin,
            full_name: request.payload.tax_details.full_name,
            desc: request.payload.tax_details.desc,
            basic_sallary: request.payload.tax_details.basic_sallary,
            total_transportation_allowance: request.payload.tax_details.total_transportation_allowance,
            total_taxable_transportation_allowance: request.payload.tax_details.total_taxable_transportation_allowance,
            over_time: request.payload.tax_details.over_time,
            other_taxable_benefites: request.payload.tax_details.other_taxable_benefites,
            taxableincome: request.payload.tax_details.taxableincome,
            tax_withheld: request.payload.tax_details.tax_withheld,
            
        });
        Incometax.save()
        .then(data => reply(data))
        .catch(err => {
          request.logger.error(err);
          return reply(Boom.internal('An error occurred. Please try again later.'));
        });
    }
    editIncometax(request,reply){
        IncometaxDataModel.findByIdAndUpdate({_id:request.payload.id},{
            number_of_transaction: request.payload.number_of_transaction,
            totalTaxebleAmount: request.payload.totalTaxebleAmount,
            totalTaxWithheld: request.payload.totalTaxWithheld,

            tax_details: request.payload.tax_details,

            tin: request.payload.tax_details.tin,
            full_name: request.payload.tax_details.full_name,
            desc: request.payload.tax_details.desc,
            basic_sallary: request.payload.tax_details.basic_sallary,
            total_transportation_allowance: request.payload.tax_details.total_transportation_allowance,
            total_taxable_transportation_allowance: request.payload.tax_details.total_taxable_transportation_allowance,
            over_time: request.payload.tax_details.over_time,
            other_taxable_benefites: request.payload.tax_details.other_taxable_benefites,
            taxableincome: request.payload.tax_details.taxableincome,
            tax_withheld: request.payload.tax_details.tax_withheld
            })
          .then(data=>!data?reply(Boom.notFound('no Incometax data')):
          reply('Incometax updated'))
                        .catch(err=>{
                            request.logger.error(err);
                            return reply(Boom.internal('An error occurred. Please try again later.'));
                        }); 
    }
    deleteIncometax(request,reply){
        IncometaxDataModel.findByIdAndRemove({_id:request.payload.id})
            .then(data=>!data?reply(Boom.notFound('no Incometax')):reply('deleted success'))
            .catch(err=>{
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            }); 
    }
    getIncometax(request,reply){
        IncometaxDataModel.find({},{__v:0,created_at:0})
            .then(data=>{
                if(!data)
                {
                return reply(Boom.notFound('no Incometax data is found'));
                }
                let result=[];
        data.map(d=>result.push({id:d._id,
            
            number_of_transaction: d.number_of_transaction,
            totalTaxebleAmount: d.totalTaxebleAmount,
            totalTaxWithheld: d.totalTaxWithheld,

           // tax_details: d.tax_details,

            tin: d.tax_details.tin,
            full_name: d.tax_details.full_name,
            desc: d.tax_details.desc,
            basic_sallary: d.tax_details.basic_sallary,
            total_transportation_allowance: d.tax_details.total_transportation_allowance,
            total_taxable_transportation_allowance: d.tax_details.total_taxable_transportation_allowance,
            over_time: d.tax_details.over_time,
            other_taxable_benefites: d.tax_details.other_taxable_benefites,
            taxableincome: d.tax_details.taxableincome,
            tax_withheld:d.tax_details.tax_withheld
              
            }))
                return reply(result);
            })
            .catch(err=>{
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            }); 
    }

}
module.exports = new IncometaxController();
