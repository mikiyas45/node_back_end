"use strict";

const Boom = require("boom"),
  // Model = require('../model'),
  Model = require("../../model"),
  mongoose = require("mongoose"),
  WithholdingDataModel = Model.taxModel;
class WithholdingController {
  createWithholding(request, reply) {
    const withholding = new WithholdingDataModel({
      _id: new mongoose.Types.ObjectId(),
      // taxpayer_identification:request.payload.taxpayer_identificationId,
      tin: request.payload.tin,
      totalTaxebleAmount: request.payload.totalTaxebleAmount,
      totalTaxWithheld: request.payload.totalTaxWithheld,
      tax_details: request.payload.tax_details
      /*   name: request.payload.name,
      tax_type: request.payload.tax_type,
      tax_period: request.payload.tax_period,
      tax_year: request.payload.tax_year,

      description: request.payload.description,

      number_of_transaction: request.payload.number_of_transaction,
      totalTaxebleAmount: request.payload.totalTaxebleAmount,
      totalTaxWithheld: request.payload.totalTaxWithheld, */

      //tax_details: request.payload.tax_details,
      // code: request.payload.tax_details.code,
      /*  type: request.payload.tax_details.type,
      code_name: request.payload.tax_details.code_name,
      desc: request.payload.tax_details.desc,
      taxableincome: request.payload.tax_details.taxableincome,
      tax_withheld: request.payload.tax_details.tax_withheld  */

      /* tin: request.payload.tax_details.tin,
            full_name: request.payload.tax_details.full_name,
            address: request.payload.tax_details.address,
            recieptNo:request.payload.tax_details.recieptNo,
            amountOfPayment:request.payload.tax_details.amountOfPayment,
            checkNumber:request.payload.tax_details.checkNumber,
            chashiersName:request.payload.tax_details.chashiersName,
            taxableincome: request.payload.tax_details.taxableincome,
            tax_withheld: request.payload.tax_details.tax_withheld,
            */
    });
    withholding
      .save()
      .then(data => reply(data))
      .catch(err => {
        request.logger.error(err);
        return reply(
          Boom.internal("An error occurred. Please try again later.")
        );
      });
  }
  editWithholding(request, reply) {
    WithholdingDataModel.findByIdAndUpdate(
      { _id: request.payload.id },
      {
        number_of_transaction: request.payload.number_of_transaction,
        totalTaxebleAmount: request.payload.totalTaxebleAmount,
        totalTaxWithheld: request.payload.totalTaxWithheld,

        tax_details: request.payload.tax_details,

        tin: request.payload.tax_details.tin,
        full_name: request.payload.tax_details.full_name,
        address: request.payload.tax_details.address,
        recieptNo: request.payload.tax_details.recieptNo,
        amountOfPayment: request.payload.tax_details.amountOfPayment,
        checkNumber: request.payload.tax_details.checkNumber,
        chashiersName: request.payload.tax_details.chashiersName,
        taxableincome: request.payload.tax_details.taxableincome,
        tax_withheld: request.payload.tax_details.tax_withheld
      }
    )
      .then(data =>
        !data
          ? reply(Boom.notFound("no withholding data"))
          : reply("withhold updated")
      )
      .catch(err => {
        request.logger.error(err);
        return reply(
          Boom.internal("An error occurred. Please try again later.")
        );
      });
  }
  deleteWithholding(request, reply) {
    WithholdingDataModel.findByIdAndRemove({ _id: request.payload.id })
      .then(data =>
        !data ? reply(Boom.notFound("no withhold")) : reply("deleted success")
      )
      .catch(err => {
        request.logger.error(err);
        return reply(
          Boom.internal("An error occurred. Please try again later.")
        );
      });
  }
  getWithholding(request, reply) {
    WithholdingDataModel.find({}, { __v: 0, created_at: 0 })
      .then(data => {
        if (!data) {
          return reply(Boom.notFound("no withhold data is found"));
        }
        let result = [];
        data.map(d =>
          result.push({
            id: d._id,
            taxPayer: d.taxPayer,
            number_of_transaction: d.number_of_transaction,
            totalTaxebleAmount: d.totalTaxebleAmount,
            totalTaxWithheld: d.totalTaxWithheld,

            //tax_details: d.tax_details,
            /*   tin: d.tax_details.tin,
                    full_name: d.tax_details.full_name,
                    address: d.tax_details.address,
                    recieptNo:d.tax_details.recieptNo,
                    amountOfPayment:d.tax_details.amountOfPayment,
                    checkNumber:d.tax_details.checkNumber,
                    chashiersName:d.tax_details.chashiersName,
                    */
            code: d.tax_details.code,
            type: d.tax_details.type,
            code_name: d.tax_details.code_name,
            desc: d.tax_details.desc,
            taxableincome: d.tax_details.taxableincome,
            tax_withheld: d.tax_details.tax_withheld
          })
        );
        return reply(result);
      })
      .catch(err => {
        request.logger.error(err);
        return reply(
          Boom.internal("An error occurred. Please try again later.")
        );
      });
  }
}
module.exports = new WithholdingController();
