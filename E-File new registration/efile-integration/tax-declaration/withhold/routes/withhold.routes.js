'use strict';

const Controller = require('../controller'),
           WithholdingController=Controller.WithholdingCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/withholding/save',
        method:'POST',
        handler:WithholdingController.createWithholding
    },
    {
        path:'/withholding/info',
        method:'GET',
        handler:WithholdingController.getWithholding
    },
    {
        path:'/withholding/update',
        method:'PUT',
        handler:WithholdingController.editWithholding
       
    },
    {
        path:'/withholding/remove',
        method:'DELETE',
        handler:WithholdingController.deleteWithholding
        }
]; 
