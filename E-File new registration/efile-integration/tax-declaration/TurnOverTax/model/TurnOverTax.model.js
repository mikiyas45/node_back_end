'use strict';

const  mongoose=require('mongoose');
mongoose.Promise = global.Promise; 

const Schema = mongoose.Schema;
const TurnOverTaxSchema=new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    //tin: { type: mongoose.Schema.Types.ObjectId, ref: 'incometax_identification' },
    
    taxpayer_name: String,
    tin: String,
    address:String,
    tax_acountNumber:Number,
    tax_center: String, 
    tax_period: String,
    phone:String,
   fax_number:String,
   document_number:String,
   description: String, 
//section 3
    sales_subject_to_2_turnoverTax:Number,
    sales_subject_to_10_turnoverTax:Number,
    turnoverTax_on_sales_subject_to_2_rate:Number,
    turnoverTax_on_sales_subject_to_10_rate:Number,
    total_turnover_tax_payable:Number,
            
            
    TurnOverTax_details: {
          catagory_of_good_and_service_subject_to_2: { type: Number, required: false },
          taxeble_turnover_tax_subject_to_2: { type: Number, required: false },
          catagory_of_good_and_service_subject_to_10: { type: Number, required: false },
          taxeble_turnover_tax_subject_to_10: { type: Number, required: false },
        
    } 
} 
    , {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });

module.exports= mongoose.model('TurnOverTax',TurnOverTaxSchema);
 