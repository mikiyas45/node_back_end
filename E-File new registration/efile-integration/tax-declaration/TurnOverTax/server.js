'use strict';

const Hapi = require('hapi'),
    Mongoose = require('mongoose'),
    Config = require('../../../config'),
    //Pack = require('../package'),
  //const Joi = require('joi'),
    server = new Hapi.Server()


server.connection({
    port: Config.server.TurnOverTax_api_port,
    routes: { cors: true }
}); 
 
server.route(require('./routes'));
server.start((err) => {
    if (err) {
        console.error(err)
        process.exit(1)
    }
    Mongoose.connect(Config.mongodb.conString, { useMongoClient: true });
    console.log('Server listening on ' + server.info.uri);
}
);