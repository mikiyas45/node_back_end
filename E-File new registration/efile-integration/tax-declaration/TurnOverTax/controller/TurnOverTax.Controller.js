'use strict';

const Boom = require('boom'),
    //Model = require('../model'),
    Model = require('../model'),
    mongoose = require('mongoose'),
    // Config = require('../../config')
    TurnOverTaxData = Model.TurnOverTaxDataModel

class TurnOverTax {
    createTurnOverTax(request, reply) {
        const TurnOverTax = new TurnOverTaxData({
            _id: new mongoose.Types.ObjectId(),
            taxpayer_name: request.payload.taxpayer_name,
            tin: request.payload.tin,
            address:request.payload.address,
            tax_acountNumber: request.payload.tax_acountNumber,
            tax_center: request.payload.tax_center,
            tax_period: request.payload.tax_period,
            phone:request.payload.phone,
           fax_number:request.payload.fax_number,
           document_number:request.payload.document_number,
           description: request.payload.description, 
  
            sales_subject_to_2_turnoverTax:request.payload.sales_subject_to_2_turnoverTax,
            sales_subject_to_10_turnoverTax:request.payload.sales_subject_to_10_turnoverTax,
            turnoverTax_on_sales_subject_to_2_rate:request.payload.turnoverTax_on_sales_subject_to_2_rate,
            turnoverTax_on_sales_subject_to_10_rate:request.payload.turnoverTax_on_sales_subject_to_10_rate,
            total_turnover_tax_payable:request.payload.total_turnover_tax_payable,
 
            TurnOverTax_details: request.payload.TurnOverTax_details,

            catagory_of_good_and_service_subject_to_2:request.payload.TurnOverTax_details.catagory_of_good_and_service_subject_to_2,
            taxeble_turnover_tax_subject_to_2:request.payload.TurnOverTax_details.taxeble_turnover_tax_subject_to_2,
            catagory_of_good_and_service_subject_to_10:request.payload.TurnOverTax_details.catagory_of_good_and_service_subject_to_10,
            taxeble_turnover_tax_subject_to_10:request.payload.TurnOverTax_details.taxeble_turnover_tax_subject_to_10

        });
        TurnOverTax.save()
            .then(data => reply(data))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    editTurnOverTax(request, reply) {
        TurnOverTaxData.findByIdAndUpdate({ _id: request.payload.id }, {

            taxpayer_name: request.payload.taxpayer_name,
            tin: request.payload.tin,
            address:request.payload.address,
            tax_acountNumber: request.payload.tax_acountNumber,
            tax_center: request.payload.tax_center,
            tax_period: request.payload.tax_period,
            phone:request.payload.phone,
           fax_number:request.payload.fax_number,
           document_number:request.payload.document_number,
           description: request.payload.description, 
 //section 3
            sales_subject_to_2_turnoverTax:request.payload.sales_subject_to_2_turnoverTax,
            sales_subject_to_10_turnoverTax:request.payload.sales_subject_to_10_turnoverTax,
            turnoverTax_on_sales_subject_to_2_rate:request.payload.turnoverTax_on_sales_subject_to_2_rate,
            turnoverTax_on_sales_subject_to_10_rate:request.payload.turnoverTax_on_sales_subject_to_10_rate,
            total_turnover_tax_payable:request.payload.total_turnover_tax_payable,

            TurnOverTax_details: request.payload.TurnOverTax_details,

            catagory_of_goog_and_service_subject_to_2:request.payload.catagory_of_goog_and_service_subject_to_2,
            taxeble_turnover_tax_subject_to_2:request.payload.taxeble_turnover_tax_subject_to_2,
            catagory_of_goog_and_service_subject_to_10:request.payload.catagory_of_goog_and_service_subject_to_10,
            taxeble_turnover_tax_subject_to_10:request.payload.taxeble_turnover_tax_subject_to_10,
        })
            .then(data => !data ? reply(Boom.notFound('no costsharing data is saved')) : reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    deleteTurnOverTax(request, reply) {
        TurnOverTaxData.findByIdAndRemove({ _id: request.payload.id })
            .then(data => !data ? reply(Boom.notFound('no costsharing')) : reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    getTurnOverTax(request, reply) {
        TurnOverTaxData.find({}, { __v: 0, created_at: 0 })
            .then(data => {
                if (!data) {
                    return reply(Boom.notFound('no success'));
                }
                let result = [];
                data.map(d => result.push({
                    id: d._id,
                    taxpayer_name:d.taxpayer_name,
                    tin: d.tin,
                    address:d.address,
                    tax_acountNumber: d.tax_acountNumber,
                    tax_center: d.tax_center,
                    tax_period: d.tax_period,
                    phone:d.phone,
                   fax_number:d.fax_number,
                   document_number:d.document_number,
                   description: d.description, 
         //section 3
                    sales_subject_to_2_turnoverTax:d.sales_subject_to_2_turnoverTax,
                    sales_subject_to_10_turnoverTax:d.sales_subject_to_10_turnoverTax,
                    turnoverTax_on_sales_subject_to_2_rate:d.turnoverTax_on_sales_subject_to_2_rate,
                    turnoverTax_on_sales_subject_to_10_rate:d.turnoverTax_on_sales_subject_to_10_rate,
                    total_turnover_tax_payable:d.total_turnover_tax_payable,
        
        
        
                    
        
                    TurnOverTax_details: request.payload.TurnOverTax_details,
        
                    catagory_of_goog_and_service_subject_to_2:d.TurnOverTax_details.catagory_of_goog_and_service_subject_to_2,
                    taxeble_turnover_tax_subject_to_2:d.TurnOverTax_details.taxeble_turnover_tax_subject_to_2,
                    catagory_of_goog_and_service_subject_to_10:d.TurnOverTax_details.catagory_of_goog_and_service_subject_to_10,
                    taxeble_turnover_tax_subject_to_10:d.TurnOverTax_details.taxeble_turnover_tax_subject_to_10,
                }))
                return reply(result);
            })
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }

}
module.exports = new TurnOverTax();