'use strict';

const Controller = require('../controller'),
TurnOverTax=Controller.TurnOverTaxCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/turnovertax/info',
        method:'GET',
        handler:TurnOverTax.getTurnOverTax
    },
    {
        path:'/turnovertax/save', 
        method:'POST',
        handler:TurnOverTax.createTurnOverTax
    },
    {
        path:'/turnovertax/update',
        method:'PUT',
        handler:TurnOverTax.editTurnOverTax
       
    },
    {
        path:'/turnovertax/remove',
        method:'DELETE',
        handler:TurnOverTax.deleteTurnOverTax
        } 
]; 
