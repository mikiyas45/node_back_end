'use strict';

const Controller = require('../controller'),
registration=Controller.registrationCtl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/registration/info',
        method:'GET',
        handler:registration.getregistration
    },
    {
        path:'/registration/save', 
        method:'POST',
        handler:registration.createregistration
    },
    {
        path:'/registration/update',
        method:'PUT',
        handler:registration.editregistration
       
    },
    {
        path:'/registration/remove',
        method:'DELETE',
        handler:registration.deleteregistration
        } 
]; 
