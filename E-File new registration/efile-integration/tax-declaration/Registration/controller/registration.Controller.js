'use strict';

const Boom = require('boom'),
Model = require('../model'),
//Model = require('../../model'),
    mongoose = require('mongoose'),
    // Config = require('../../config')
    registrationData = Model.registrationModel

class registration {
    createregistration(request, reply) {
        const registration = new registrationData({
            _id: new mongoose.Types.ObjectId(),
            first_name: request.payload.first_name,
            last_name: request.payload.last_name,
            username: request.payload.username,
            email:request.payload.email, 
            mobile_number:request.payload.mobile_number,
            employee_id:request.payload.employee_id
           // is_one_time_password_set:true

        });
        registration.save()
            .then(data => reply(data))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    editregistration(request, reply) {
        registrationData.findByIdAndUpdate({ _id: request.payload.id }, {
           
            first_name: request.payload.first_name,
            last_name: request.payload.last_name,
            username: request.payload.username,
            email:request.payload.email, 
            mobile_number:request.payload.mobile_number,
            employee_id:request.payload.employee_id,
           // is_one_time_password_set:true


        })
            .then(data => !data ? reply(Boom.notFound('no costsharing data is saved')) : reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    deleteregistration(request, reply) {
        registrationData.findByIdAndRemove({ _id: request.payload.id })
            .then(data => !data ? reply(Boom.notFound('no costsharing')) : reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    getregistration(request, reply) {
        registrationData.find({}, { __v: 0, created_at: 0 })
            .then(data => {
                if (!data) {
                    return reply(Boom.notFound('no success'));
                }
                let result = [];
                data.map(d => result.push({
                    id: d._id,
                    
                    first_name: d.first_name,
                    last_name: d.last_name,
                    username: d.username,
                    email:d.email, 
                    mobile_number:d.mobile_number,
                    employee_id:d.employee_id, 
                   // is_one_time_password_set:true

                }))
                return reply(result);
            })
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }

}
module.exports = new registration();