'use strict';

const  mongoose=require('mongoose');
mongoose.Promise = global.Promise; 

const Schema = mongoose.Schema;
const registrationSchema=new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    first_name: String,
    last_name: String,
    username: String,
    email:String, 
    mobile_number:Number,
    employee_id:Number,
   // is_one_time_password_set:true

    
}
    , {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    });

module.exports= mongoose.model('regestration',registrationSchema);
