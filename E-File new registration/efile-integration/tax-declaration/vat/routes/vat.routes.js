'use strict';

const Controller = require('../controller'),
               taxcontroller=Controller.taxctl
      //Config = require('../../config'),
     // userValidation = require('../validation');

module.exports=[
    {
        path:'/tax/save',
        method:'POST',
        handler:taxcontroller.createtax
    },
    {
        path:'/tax/info',
        method:'GET',
        handler:taxcontroller.gettax
    }, 
    { 
        path:'/tax/update',
        method:'PUT',
        handler:taxcontroller.edittax
        
    }, 
    { 
        path:'/tax/remove',
        method:'DELETE', 
        handler:taxcontroller.deletetax
        }
]; 

