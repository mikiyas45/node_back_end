'use strict';

const Boom = require('boom'),
    //Model = require('../model'),
    Model = require('../../model'),
    mongoose = require('mongoose'),
    taxModel = Model.taxModel

class TaxController {

    createtax(request, reply) {
        const tax = new taxModel({
            _id: new mongoose.Types.ObjectId(),
            // tin: request.payload.tin,
            // name: request.payload.name,
            // tax_type: request.payload.tax_type,
            // tax_period: request.payload.tax_period,
            // tax_year: request.payload.tax_year,

            // description: request.payload.description,
            // number_of_transaction: request.payload.number_of_transaction,
            // totalTaxeble: request.payload.totalTaxeble,
            // totalTaxWithheld: request.payload.totalTaxWithheld,
             //type: request.payload.tax_details.type,
             tax_detail : request.payload.tax_detail,
           
             
           /*  tax_details: request.payload.tax_details,

            code: request.payload.tax_details.code,
            type: request.payload.tax_details.type,
            code_name: request.payload.tax_details.code_name,
            desc: request.payload.tax_details.desc,
            taxableincome: request.payload.tax_details.taxableincome,
            tax_withheld: request.payload.tax_details.tax_withheld */

        });
        tax.save()
            .then(() => reply('success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }


    edittax(request, reply) {
        taxModel.findByIdAndUpdate({_id: request.payload.id }, {
            tin: request.payload.tin,
            name: request.payload.name,
            tax_type: request.payload.tax_type,
            tax_period: request.payload.tax_period,
            tax_year: request.payload.tax_year,
            description: request.payload.description,
            number_of_transaction: request.payload.number_of_transaction,
            totalTaxeble: request.payload.totalTaxeble,
            totalTaxWithheld: request.payload.totalTaxWithheld,
            tax_details: request.payload.tax_details,

            code: request.payload.tax_details.code,
            type: request.payload.tax_details.type,
            code_name: request.payload.tax_details.code_name,
            desc: request.payload.tax_details.desc,
            taxableincome: request.payload.tax_details.taxableincome,
            tax_withheld: request.payload.tax_details.tax_withheld,


        })
            .then(data => !data ? reply(Boom.notFound('no data')) : 
                reply('crud updated'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }
    deletetax(request, reply) {
        taxModel.findByIdAndRemove({ _id: request.payload.id })
            .then(data => !data ? reply(Boom.notFound('no tax')) : reply('deleted success'))
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    } 
    gettax(request, reply) {
        taxModel.find({}, { __v: 0, created_at: 0 })
            .then(data => {
                if (!data) {
                    return reply(Boom.notFound('no crud data is found'));
                }
                let result = [];
                data.map(d => result.push({
                    id: d._id,
                    tin: d.tin,
                    name: d.name,
                    tax_type: d.tax_type,
                    tax_period: d.tax_period,
                    tax_year: d.tax_year,
                    description: d.description,
                    number_of_transaction: d.number_of_transaction,
                    totalTaxeble: d.totalTaxeble,
                    totalTaxWithheld: d.totalTaxWithheld,
                   // tax_details: d.tax_details,
                    code: d.tax_details.code,
                    type: d.tax_details.type,
                    code_name: d.tax_details.code_name,
                    desc: d.tax_details.desc,
                    taxableincome: d.tax_details.taxableincome,
                    tax_withheld: d.tax_details.tax_withheld,



                }))
                return reply(result);
            })
            .catch(err => {
                request.logger.error(err);
                return reply(Boom.internal('An error occurred. Please try again later.'));
            });
    }

}
module.exports = new TaxController();
