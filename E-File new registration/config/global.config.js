'use strict';
module.exports = {
  schemes: {
    $filter: 'env',
    prod: ['https', 'http'],
    $default: ['http']
  },
  server: {
    //customer_api_port:5010,
     registration_api_port :5010,
     
      costsharing_api_port :5011,
      incometax_api_port :5012,
      vat_api_port :5013,
      withhold_api_port :5014,
      pension_api_port :5015,
     upload_api_port :5016,
     excise_api_port :5017,
     TurnOverTax_api_port:5018,
  
  },
  mongodb: { 
    conString: {
      $filter: 'env',
      prod: {
        $filter: 'zone',
        zone1: 'mongodb://api-user:password@172.21.51.88:27017/efiledb',
        zone2: 'mongodb://api-user:password@172.21.51.88:27017/efiledb',
        $default: 'mongodb://api-user:password@172.21.51.88:27017/efiledb'
      },
      staging: 'mongodb://172.21.51.88:27017/efiledb',
      qa: 'mongodb://127.0.0.1:27017/efiledb',
      dev: 'mongodb://172.21.51.88:27017/efiledb',
      $default: 'mongodb://172.21.35.1:27017/efiledb'
    }
  },
  baseUrl:{
    $filter:'env',
    prod:{
        api_base_url:'https://172.21.35.1/',
        short_code_base_url:'m.derash.gov.et'
    },
    $default:{
        api_base_url:'http://192.168.0.11/',
        short_code_base_url:'http://192.168.0.12/'
    }
}
};
