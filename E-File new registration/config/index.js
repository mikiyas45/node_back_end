'use strict';

const Confidence = require('confidence');
const deployment_location= require('./env.config');
const globalConfig= require('./global.config');
const Config = {
  bill_data_file:{
      maxUploadSize:20971520034314,
      upload_base_folder:{
          $filter:'env',
          qa:'/C:/Users/user/Documents/efileDoc'
        
      }
  }
  
};
const store = new Confidence.Store(Object.assign(globalConfig,Config));

module.exports=store.get('/',deployment_location);