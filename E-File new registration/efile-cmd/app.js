'use strict';

const program = require('commander');
const {prompt} = require('inquirer');
const { generateAPIKey,apiKeyQuestions } = require('./generate-api-key');
const { createAdminUser,adminUserQuestions } = require('./admin.user');


program
    .version('0.0.1')
    .description('Derash cmd tool');
program
    .command("generateAPIKey")
    .alias("gak")
    .description("Generate Stakeholders API Key to use Derash API ")
    .action(()=>{
        prompt(apiKeyQuestions).then(answers=>
         generateAPIKey(answers,(err,apiCredential)=>{
                if(err) throw err;
                console.log(apiCredential)
            }));
    });

program
    .command("createAdminUser")
    .alias("cau")
    .description("Create the first admin user")
    .action(()=>{
        prompt(adminUserQuestions).then(answers=>createAdminUser(answers));
    });


program.parse(process.argv);