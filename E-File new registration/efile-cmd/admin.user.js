'use strict';

const Mongoose = require('mongoose'),
      Config = require('../config');
Mongoose.connect(Config.mongodb.conString,{useMongoClient: true});

const User = require('../derash-web-auth-api/model').UserModel;

const adminUserQuestions=[
    {
        type:'input',
        name:'first_name',
        message:'Enter First Name: ',
        validate:function(input){
            if(input.trim()!==''&&input.length>=3){
                return true;
            }
            return 'Please enter first name with three character or more';
        }
    },  
    {
        type:'input',
        name:'last_name',
        message:'Enter last Name: ',
        validate:function(input){
            if(input.trim()!==''&&input.length>=3){
                return true;
            }
            return 'Please enter last name with three character or more';
        }
    },  
    {
        type:'input',
        name:'email',
        message:'Enter email: ',
        validate:function(input){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(re.test(String(input).toLowerCase())){
                return true;
            }
            return 'Please enter valid email';
        }
    },     
    {
        type:'input',
        name:'mobile_number',
        message:'Enter mobile number: ',
        validate: function(value) {
            const pass = value.match(
              /^09(\d{8})$/i
            );
            if (pass) {
              return true;
            }
            return 'Please enter a valid mobile number starting with 09';
          }
    },
    {
        type:'password',
        name:'password',
        message:'Enter Password: ',
        validate:function(input){
            if(input.trim()!==''&&input.length>=8){
                return true;
            }
            return 'Please enter password with eight character or more';
        }
    }
    ];   
const createAdminUser = (userData)=>{
    
    let user = new User({
            first_name: userData.first_name,
            last_name: userData.last_name,
            username: userData.email,
            email:userData.email, 
            mobile_number:userData.mobile_number,
            password:userData.password,
            is_verified:true,
            scope:["ADMIN"]
        });
    user.save()
        .then(()=>console.log(`${userData.email} created successfully`))
        .catch(err=>console.log(err));
    }
module.exports={createAdminUser,adminUserQuestions};