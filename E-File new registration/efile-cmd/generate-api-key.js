'use strict';

const Token = require('../util').Token;
const Config = require('../config');

const inquirer = require('inquirer');
const apiSecret = Math.random().toString(36).substr(2, 9)+Math.random().toString(36).substr(2, 9)+Math.random().toString(36).substr(2, 9)+Math.random().toString(36).substr(2, 9);
const Mongoose = require('mongoose');
const StakeholderApiKeyModel=require('../model').StakeholderApiKeyModel;

const apiKeyQuestions=[
    {
        type:'list',
        name:'type',
        message:'Generate Key for? ',
        choices:[ "BILLER", new inquirer.Separator(), "AGENT" ]
    },    
    {
    type:'input',
    name:'name',
    message:'Enter name of stakeholder: '
    },
    {
        type:'input',
        name:'stakeholderId',
        message:'Enter stakeholder Id: '
    }
    ];
//note: generate Token on the env you want to be used.
const generateAPIKey = ({name,type,stakeholderId},cb)=>{
    const apiCredential= {
        apiSecret:apiSecret,
        apiKey:Token.generateAPIKey({
            name:name,
            sub:stakeholderId, 
            permissions:[type]
        },apiSecret)
    };
    // const stakeholderApiKeyModel = new StakeholderApiKeyModel({
    //     api_key_id:Token.decode(apiCredential.apiKey).jti,
    //     stakeholder_id:stakeholderId,
    //     stakeholder_name:name
    // });
    // Mongoose.connect(Config.mongodb.conString,{useMongoClient: true});
    // stakeholderApiKeyModel.save()
    //     .then(_=>{
    //         cb(null,apiCredential);
    //     })
    //     .catch(err=>cb(err,null))
}
module.exports = { generateAPIKey,apiKeyQuestions };