'use strict';

const FileManager=require('./upload-file');

module.exports = {
  upload:FileManager.upload,
  FileManager:require('./file-manager.util')
};