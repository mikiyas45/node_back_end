'use strict'

let fs = require('fs'),
    multiparty = require('multiparty'),
    dateTime = require('node-datetime'),
   shortid = require('shortid'),
    Config = require('../config');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$');

class FileManager {
    upload(request,cb){
        
       let form = new multiparty.Form();
        form.parse(request.payload, function(err, fields, files) {
            if (err)  
            {
                cb(err);
            }
            else {
                
                fields.billerId=[request.payload.biller_id];
                fields.billerName=[request.payload.biller_name];
                fields.userId=[request.payload.userName];
                fields.userName=[request.payload.full_name];
                uploadFile(fields,files, cb);
            }
        });
        
    }
    uploadLogo(request,cb){
        
       let form = new multiparty.Form();
        form.parse(request.payload, function(err, fields, files) {
            if (err)  
            {
                cb(err);
            }
            else {
                
                fields.billerId=[request.payload.biller_id];
                fields.billerName=[request.payload.biller_name];
                fields.userId=[request.payload.userName];
                fields.userName=[request.payload.full_name];
                uploadFile(fields,files, cb);
            }
        });
        
    }
    // checkFileExist(billDataPath,fullName,data,writeFile,cb) {
    //     fs.exists(fullName, function(exists) {
    //         if (exists === false) 
    //         {
    //             let mkdirp = require('mkdirp');
    //             mkdirp(billDataPath,function(err){
    //                 if(err) writeFile(err);
    //                 else writeFile(null,fullName,data,cb);
    //             });
    //         }
        
    //     });
    // }

}
let prepareDestinationFolder = function(billerId){
    return Config.uploadFolder.billDataFolder+billerId+'/'+dateTime.create().format('Y-m-d')+'/'+shortid.generate();
}


let uploadFile = function(fields,files, cb) {

    
    fs.readFile(files.file[0].path, function(err, data) {

            if (err)  
            {
                console.error(err);
                cb(err);
            }
            else
            {
                let billerId=fields.billerId&&fields.billerId[0];
                let destFolder=prepareDestinationFolder(billerId);

                let metaData={
                                billerId: billerId,
                                billerName:fields.billerName&&fields.billerName[0],
                                billUploadId: destFolder.substring(destFolder.lastIndexOf('/')+1),
                                billUploadedDate: new Date(),
                                billFileName: files.file[0].originalFilename,
                                billUploaderId: fields.userId&&fields.userId[0],
                                billUploaderName: fields.userName&&fields.userName[0],
                                description: fields.description&&fields.description[0]
                        };
                
                writeFile(null,destFolder,metaData,data,cb);
            }
    });
};
let writeFile= function(err,destFolder,metaData,data,cb){
   let fullName=destFolder+'/'+metaData.billFileName;
   let uploadedBillDataId=metaData.billUploadId;
   fs.exists(fullName, function(exists) {
        if (exists === false) //fs.mkdirSync(billDataPath);
        {
            let oldmask = process.umask(0);
            let mkdirp = require('mkdirp');
           
            mkdirp(destFolder, '0777',function(err){
                process.umask(oldmask);
                if (err)  
                        {
                            console.error(err);
                            cb(err);
                        }
                else {
                    fs.writeFile(fullName, data, function(err) {
                        if (err)  
                        {
                            console.error(err);
                            cb(err);
                        }
                    
                        else 
                        {
                            // let Queue=require('../util/').Queue;
                            // let exchange = 'billdata';
                            // let routing_key = "bill.upload";
                            // let eOptions={durable: true,autoDelete:false};
                            // let qOptions={durable: true,autoDelete:false,exclusive:false,arguments:null};
                            // //let mOptions={durable: true};
                            // let mOptions={persistent: true,type:"et.gov.derash.backend.datacontracts.BillUploadDataContract:et.gov.derash.backend.datacontracts"};
                            // //(exchangeName,routing_key,message,mOptions,cb)
                            
                            // Queue.sendToTopicExchange(exchange, routing_key,JSON.stringify(metaData),mOptions,function(err,data){
                            //     if(err)  cb(err);
                            //     cb(null,{"uploadedBillDataId":uploadedBillDataId,"fileName": metaData.billFileName});
                            // });
                            metaData.billFilePath=fullName;
                            cb(null, {reply:{"uploadedBillDataId":uploadedBillDataId,"fileName": metaData.billFileName},metaData:metaData})
                        }
                      
                    });
                    }
                });
        }
        else
        {

            cb(null,`File ${fullName} already exist`);
        } 

    });
}

module.exports = new FileManager();