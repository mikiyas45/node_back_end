'use strict'

const fs = require('fs-extra');

class FileManager {
    //TODO: make it full promise based solution
    uploadFile(filePath,originalFilename,destinationFolder,cb){
        fs.readFile(filePath)
                .then((data)=>{
                    fs.mkdirp(destinationFolder)
                        .then(()=>{
                            let destinationFileFullName = `${destinationFolder}/${originalFilename}`;
                            fs.exists(destinationFileFullName,exists=>{
                                if(!exists){
                                    fs.writeFile(destinationFileFullName,data)
                                                .then(()=>cb(null))
                                                .catch(err=>cb(err));   
                                }
                                else{
                                    cb('File already exist');
                                }
                            });
                        })
                        .catch(error=>cb(error));
        })
        .catch(error=>cb(error));
    }
}

module.exports = new FileManager();
